import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IniciarsesionComponent } from './modulos/iniciarsesion/iniciarsesion.component';
import { ProductosComponent } from './modulos/productos/productos.component';


const routes: Routes = [
    {
      path:'iniciar-sesion',
      component: IniciarsesionComponent
    },

    {
      path:'productos',
      loadChildren:()=>import ('./modulos/productos/productos.module').then(m => m.ProductosModule)
    },
    
    {
      path:'**',
      pathMatch: 'full',
      redirectTo: 'iniciar-sesion'
    },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
