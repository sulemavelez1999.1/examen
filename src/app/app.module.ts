import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IniciarsesionComponent } from './modulos/iniciarsesion/iniciarsesion.component';
import { InputTextModule } from 'primeng/inputtext';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PasswordModule } from 'primeng/password';
import { ButtonModule } from 'primeng/button';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { UsuariosService } from './services/usuarios.service';

import { ToastrService } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { RatingModule } from 'primeng/rating';
import { NuevoproductosComponent } from '../app/modulos/productos/nuevoproductos/nuevoproductos.component';

@NgModule({
  declarations: [
    AppComponent,
    IniciarsesionComponent,
    NuevoproductosComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    InputTextModule,
    FormsModule,
    PasswordModule,
    ButtonModule,
    ReactiveFormsModule,
    RatingModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    
  ],
  providers: [
    UsuariosService,
    ToastrService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
