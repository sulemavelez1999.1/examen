import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MainProduct } from '../interface/productos.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {
  private API_URL = 'https://dummyjson.com/products'

  constructor( private http: HttpClient) { }
  
  getProductList(): Observable<MainProduct> {
    return this.http.get<MainProduct>(this.API_URL);
  }
}
