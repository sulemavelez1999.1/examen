import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {User, MainData} from '../interface/users.interface'

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {
  private API_URL = 'https://dummyjson.com/users';


  constructor(private http: HttpClient) { }
 /* getUser():any{
   const s = this.http.get('https://www.pichincha.com/portal/principal/personas/inversiones/plazodolar-en-linea?utm_source=google&utm_medium=cpc&utm_campaign=inversion&utm_content=MH-SEM-Inversiones&gclid=CjwKCAjwo7iiBhAEEiwAsIxQEVjy_7LZvgOcwA4XA2rN3LaJHJbnel_aGUsfVIb6mtFl00GNNi-dbRoCPOYQAvD_BwE')
    console.log(s)
  }
*/
  authenticate(email: string, password: string): Observable<boolean> {
    return this.http.get<MainData>(this.API_URL).pipe(
      tap(data => console.log(data)),
      map((data: MainData) => {
        console.log(data)
        for(let element of data.users){
          if(element.email === email && element.password === password){
            console.log('Ingresa con credenciales')
            return true;


          }
        }
        // Usuario no válido
        return false;
      })
    );
  }
  
  
  
  

}