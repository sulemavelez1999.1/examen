import { Component, OnInit } from '@angular/core';
import { ProductosService } from 'src/app/services/productos.service';
import {MainProduct , Product} from 'src/app/interface/productos.interface'


@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {
  productList: Product[] = [];

  /**
   *
   */
  constructor( private productosService: ProductosService) {
    
    
  }
  ngOnInit(): void {
    this.productosService.getProductList().subscribe((data: MainProduct) => {
      this.productList = data.products;
    });

  }
  

}
