import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductosComponent } from './productos.component';
import { NuevoproductosComponent } from './nuevoproductos/nuevoproductos.component';

const routes: Routes = [
  {
    path:'',
    component: ProductosComponent
  },
  {
    path: 'nuevo-producto',
    component: NuevoproductosComponent
  }
,
  {
    path:'**',
    pathMatch: 'full',
    redirectTo: ''
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductosRoutingModule { }

