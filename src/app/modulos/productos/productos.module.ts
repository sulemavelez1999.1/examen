import { NgModule } from '@angular/core';
import { ProductosComponent } from './productos.component';
import { ProductosRoutingModule } from './productos-routing.module';
import { TableModule } from 'primeng/table';
import { CommonModule } from '@angular/common';
import { RatingModule } from 'primeng/rating';
import { FormsModule } from '@angular/forms';
import { Button } from 'primeng/button';
import { ButtonModule } from 'primeng/button';

@NgModule({
  declarations: [
    ProductosComponent
  ],
  imports: [
   ProductosRoutingModule,
   TableModule,
   FormsModule,
   CommonModule,
   RatingModule,
   ButtonModule
   
   
  ], 
  exports: [CommonModule] 
})
export class ProductosModule { }
