import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoproductosComponent } from './nuevoproductos.component';

describe('NuevoproductosComponent', () => {
  let component: NuevoproductosComponent;
  let fixture: ComponentFixture<NuevoproductosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NuevoproductosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NuevoproductosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
