import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Form, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-iniciarsesion',
  templateUrl: './iniciarsesion.component.html',
  styleUrls: ['./iniciarsesion.component.css']
})
export class IniciarsesionComponent implements OnInit{
  formLogin!: FormGroup ;
  /**
   *
   */
  constructor(private fb: FormBuilder, private usuariosService: UsuariosService, private router: Router, private toastr: ToastrService) {
    
  }

  ngOnInit(): void{
    this.formLogin = this.fb.group({
      email: ['', [Validators.required, Validators.email]] ,
      password: ['',[Validators.required, Validators.minLength(8), Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/
      )]]
    });
    console.log('Iniciando sesión...');
  }

 
iniciarsesion(){
  
  console.log('Valor del formulario de inicio de sesión:', this.formLogin);
  if (this.formLogin?.valid) {
    const email = this.formLogin.get('email')?.value;
    const password = this.formLogin.get('password')?.value;

    this.usuariosService.authenticate(email, password).subscribe((authenticated: boolean) => {
      if (authenticated) {
        console.log('Usuario válido, redirigir a la página correspondiente');
        this.router.navigate(['/productos']);
      } else {
        console.log('Usuario inválido, redirigir a la página correspondiente');
        this.toastr.error('Error de inicio de sesión', 'El usuario no tiene las credenciales', );
      }
    });
    
  }
 {
  console.log('Iniciando sesión con credenciales:', this.formLogin.value);
 } 
 
  

  }
 /* getUser(){
    this.usuariosService.getUser()

  }
  */
}
